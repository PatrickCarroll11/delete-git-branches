#!/bin/bash

# Assumes remote repo 'origin'
# Parameter 0: branch filter
# Parameter 1: directory

filter=$1
directory=$2

need_to_move=$false

if [ "$filter" == "" ]; then
	echo "Filter cannot be empty."
	exit
fi

if [ "$directory" == "" ]; then
	echo "Will use current directory."
else
	echo "Going to directory $directory."
	need_to_move=$true
fi


# directory="C:\Users\dpapagiannis\Documents\workspace\lidarworkstation\lidarworkstation"
# filter="twi"

echo "Will try to delete all branches that contain \"$filter\" in repo $(pwd)."

if [ $need_to_move ]; then
	cd directory
fi

result="$(git branch | grep $filter)"

if [ "$result" == "" ]; then
	echo "No branches containing \"$filter\" were found."
	exit
fi

# Iterate in results and delete them
while IFS= read -r line;
do
	echo "Deleting branch \"$line\"..."
	echo "\tLocally"
	git push --delete origin $line		# Delete remote branch
	echo "\tRemotely"
	git branch -D $line					# Delete local branch
done <<< "$result";

