# Usage
`delete_branches.sh <filter> [<directory>]`

Where `filter` is a non-empty string which will be used to filter branch names and `directory` is an optional parameter for the repo (will use the current dir if empty).